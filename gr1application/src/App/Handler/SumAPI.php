<?php
 
declare(strict_types=1);
 
namespace App\Handler;
 
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
 
use function time;

/**
 * 
 * Класс для API суммы входных значений метода GET
 * 
 */
class SumAPI implements RequestHandlerInterface
{

    /**
     * Get query string parse values from it and returns sum of values in Json format
     * 
     * @param ServerRequestInterface
     * 
     * @return JsonResponse
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $query = $_SERVER['QUERY_STRING'];
        $vars = array();
        $sum = 0;
        foreach (explode('&', $query) as $pair) {
            list($key, $value) = explode('=', $pair);
            $vars[] = array(urldecode($key), urldecode($value));
        }
        foreach($vars as $item) {
            $sum += $item[1];
        }
        return new JsonResponse(['sum' => $sum]);

    }
}